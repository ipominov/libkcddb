# translation of kcmcddb.po to Greek
# Copyright (C) 2003, 2005, 2007 Free Software Foundation, Inc.
#
# Χαράλαμπος Κανιός <kaniosh@linuxmail.org>, 2003.
# Stergios Dramis <sdramis@egnatia.ee.auth.gr>, 2003.
# Spiros Georgaras <sng@hellug.gr>, 2005, 2007.
# Toussis Manolis <manolis@koppermind.homelinux.org>, 2007.
# Glentadakis Dimitrios <translators_team@mandrivalinux.gr>, 2009.
# Stelios <sstavra@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: kcmcddb\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-27 00:44+0000\n"
"PO-Revision-Date: 2020-08-31 11:32+0300\n"
"Last-Translator: Stelios <sstavra@gmail.com>\n"
"Language-Team: Greek <kde-i18n-el@kde.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.04.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: cddbconfigwidget.cpp:40
#, kde-format
msgid "Cache Locations"
msgstr "Θέσεις της λανθάνουσας μνήμης"

#: cddbconfigwidget.cpp:75
#, kde-format
msgid "Could not fetch mirror list."
msgstr "Αδυναμία λήψης της λίστας καρθεφτισμών."

#: cddbconfigwidget.cpp:75
#, kde-format
msgid "Could Not Fetch"
msgstr "Αδυναμία λήψης"

#: cddbconfigwidget.cpp:79
#, kde-format
msgid "Select mirror"
msgstr "Επιλογή καθρεπτισμού"

#: cddbconfigwidget.cpp:80
#, kde-format
msgid "Select one of these mirrors"
msgstr "Επιλέξτε έναν από αυτούς τους καθρεφτισμούς"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_FreedbLookupTransport)
#. i18n: ectx: property (text), widget (QRadioButton, httpButton)
#: cddbconfigwidget.cpp:97 cddbconfigwidget.ui:129 cddbconfigwidget.ui:179
#, kde-format
msgid "HTTP"
msgstr "HTTP"

#. i18n: ectx: property (text), item, widget (QComboBox, kcfg_FreedbLookupTransport)
#: cddbconfigwidget.cpp:99 cddbconfigwidget.ui:124
#, kde-format
msgid "CDDB"
msgstr "CDDB"

#. i18n: ectx: property (windowTitle), widget (QWidget, CDDBConfigWidgetBase)
#: cddbconfigwidget.ui:7
#, kde-format
msgid "CDDB Settings"
msgstr "Ρυθμίσεις CDDB"

#. i18n: ectx: attribute (title), widget (QWidget, tabLookup)
#: cddbconfigwidget.ui:20
#, kde-format
msgid "&Lookup"
msgstr "&Αναζήτηση"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_MusicBrainzLookupEnabled)
#: cddbconfigwidget.ui:26
#, kde-format
msgid "Enable MusicBrainz lookup"
msgstr "Ενεργοποίηση αναζήτησης μέσω MusicBrainz"

#. i18n: ectx: property (text), widget (QCheckBox, kcfg_FreedbLookupEnabled)
#: cddbconfigwidget.ui:33
#, kde-format
msgid "Enable freedb lookup"
msgstr "Ενεργοποίηση αναζήτησης μέσω freedb"

#. i18n: ectx: property (title), widget (QGroupBox, freedbServerBox)
#: cddbconfigwidget.ui:40
#, kde-format
msgid "Freedb Server"
msgstr "Εξυπηρετητής Freedb"

#. i18n: ectx: property (text), widget (QLabel, TextLabel7)
#: cddbconfigwidget.ui:46
#, kde-format
msgid "Freedb server:"
msgstr "Εξυπηρετητής freedb:"

#. i18n: ectx: property (whatsThis), widget (QLineEdit, kcfg_hostname)
#: cddbconfigwidget.ui:64
#, kde-format
msgid "Name of CDDB server which will be used to look up CD information."
msgstr ""
"Το όνομα του εξυπηρετητή CDDB που θα χρησιμοποιηθεί για την αναζήτηση "
"πληροφοριών CD."

#. i18n: ectx: property (text), widget (QLineEdit, kcfg_hostname)
#: cddbconfigwidget.ui:67
#, kde-format
msgid "gnudb.gnudb.org"
msgstr "gnudb.gnudb.org"

#. i18n: ectx: property (text), widget (QLabel, textLabel3_3)
#. i18n: ectx: property (text), widget (QLabel, textLabel3_2)
#. i18n: ectx: property (text), widget (QLabel, textLabel3)
#: cddbconfigwidget.ui:80 cddbconfigwidget.ui:208 cddbconfigwidget.ui:241
#, kde-format
msgid "Port:"
msgstr "Θύρα:"

#. i18n: ectx: property (whatsThis), widget (QSpinBox, kcfg_port)
#: cddbconfigwidget.ui:93
#, kde-format
msgid "Port to connect to on CDDB server."
msgstr "Θύρα για σύνδεση στον εξυπηρετητή CDDB."

#. i18n: ectx: property (text), widget (QLabel, TextLabel9)
#: cddbconfigwidget.ui:108
#, kde-format
msgid "&Transport:"
msgstr "&Μεταφορά:"

#. i18n: ectx: property (whatsThis), widget (QComboBox, kcfg_FreedbLookupTransport)
#: cddbconfigwidget.ui:120
#, kde-format
msgid "Type of lookup which should be tried at the CDDB server."
msgstr "Τύπος αναζήτησης που πρέπει να δοκιμαστεί στον εξυπηρετητή CDDB."

#. i18n: ectx: property (text), widget (QPushButton, mirrorListButton)
#: cddbconfigwidget.ui:137
#, kde-format
msgid "Show &Mirror List"
msgstr "Εμφάνιση λίστας καθρεπτισ&μών"

#. i18n: ectx: attribute (title), widget (QWidget, tabSubmit)
#: cddbconfigwidget.ui:153
#, kde-format
msgid "&Submit"
msgstr "&Υποβολή"

#. i18n: ectx: property (text), widget (QLabel, textLabel1)
#: cddbconfigwidget.ui:161
#, kde-format
msgid "Email address:"
msgstr "Διεύθυνση email:"

#. i18n: ectx: property (title), widget (QGroupBox, kcfg_FreedbSubmitTransport)
#: cddbconfigwidget.ui:173
#, kde-format
msgid "Submit Method"
msgstr "Μέθοδος υποβολής"

#. i18n: ectx: property (text), widget (QLabel, textLabel2)
#: cddbconfigwidget.ui:189
#, kde-format
msgid "Server:"
msgstr "Εξυπηρετητής:"

#. i18n: ectx: property (text), widget (QRadioButton, smtpButton)
#: cddbconfigwidget.ui:220
#, kde-format
msgid "SMTP (Email)"
msgstr "SMTP (Email)"

#. i18n: ectx: property (text), widget (QCheckBox, needsAuthenticationBox)
#: cddbconfigwidget.ui:266
#, kde-format
msgid "Server needs authentication"
msgstr "Ο διακομιστής απαιτεί ταυτοποίηση"

#. i18n: ectx: property (text), widget (QLabel, textLabel9_2)
#: cddbconfigwidget.ui:286
#, kde-format
msgid "Reply-To:"
msgstr "Απάντηση σε:"

#. i18n: ectx: property (text), widget (QLabel, textLabel2_2)
#: cddbconfigwidget.ui:299
#, kde-format
msgid "SMTP server:"
msgstr "Διακομιστής SMTP:"

#. i18n: ectx: property (text), widget (QLabel, textLabel4)
#: cddbconfigwidget.ui:306
#, kde-format
msgid "Username:"
msgstr "Όνομα χρήστη:"

#: kcmcddb.cpp:41
#, kde-format
msgid ""
"CDDB is used to get information like artist, title and song-names in CD's"
msgstr ""
"Το CDDB χρησιμοποιείται για τη λήψη πληροφοριών όπως καλλιτέχνης, τίτλος και "
"ονόματα τραγουδιών των CD"

#: kcmcddb.cpp:69
#, kde-format
msgid ""
"freedb has been set to use HTTP for submissions because the email details "
"you have entered are incomplete. Please review your email settings and try "
"again."
msgstr ""
"Οι υποβολές freedb μέσω SMTP έχουν απενεργοποιηθεί\n"
"γιατί οι λεπτομέρειες email που δώσατε είναι ελλιπείς. Παρακαλώ ελέγξτε τις "
"ρυθμίσεις email σας και προσπαθήστε ξανά."

#: kcmcddb.cpp:72
#, kde-format
msgid "Incorrect Email Settings"
msgstr "Εσφαλμένες ρυθμίσεις Email"

#~ msgid "freedb.freedb.org"
#~ msgstr "freedb.freedb.org"

#~ msgid "&Port:"
#~ msgstr "&Θύρα:"
